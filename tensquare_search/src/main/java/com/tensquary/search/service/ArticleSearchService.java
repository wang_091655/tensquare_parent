package com.tensquary.search.service;


import com.tensquary.search.dao.ArticleSearchDao;
import com.tensquary.search.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by progr on 2019/3/12.
 */
@Service
public class ArticleSearchService {

    @Autowired
    private ArticleSearchDao articleSearchDao;

    /**
     * 增加文章
     */
    public void add(Article article) {
        articleSearchDao.save(article);
    }

    /**
     * 检索文章
     */
    public Page<Article> findbyTitleLike(String keywords, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return articleSearchDao.findByTitleOrContentLike(keywords, keywords, pageable);
    }
}
