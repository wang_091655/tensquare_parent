package com.tensquary.search.dao;


import com.tensquary.search.pojo.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by progr on 2019/3/12.
 */
public interface ArticleSearchDao extends ElasticsearchRepository<Article, String> {

    /**
     * 检索文章
     */
    public Page<Article> findByTitleOrContentLike(String title, String content, Pageable pageable);


}
