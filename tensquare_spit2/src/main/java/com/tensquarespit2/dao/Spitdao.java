package com.tensquarespit2.dao;

import com.tensquarespit2.pojo.Spit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface Spitdao extends MongoRepository<Spit, String> {
    public Page<Spit> findByParentid(String parentid, Pageable pageable);


}
