package com.bw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class rabbitApplication {
    public static void main(String[] args) {
        SpringApplication.run(rabbitApplication.class);
    }
}
