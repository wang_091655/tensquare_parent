package com.tensquare.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import util.IdWorker;
import util.JwtUtil;

import javax.rmi.CORBA.Util;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		//王建修改了
        //xuqing修改了
		SpringApplication.run(Application.class, args);
		/*BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		Boolean aaa=bCryptPasswordEncoder.matches("$2a$10$swu8s7qT4LJxbnuyZAvxyuXhRenGKTOMqvt59cfu9XMfKlZjg8zRO","$2a$10$swu8s7qT4LJxbnuyZAvxyuXhRenGKTOMqvt59cfu9XMfKlZjg8zRO");
		System.out.println(aaa);*/
	}

	@Bean
	public IdWorker idWorkker(){
		return new IdWorker(1, 1);
	}
	@Bean
	public BCryptPasswordEncoder bcryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}
	@Bean
	public JwtUtil jwtUtil(){
		return new util.JwtUtil();
	}


}
